/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.base;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;
import static com.ail.pageflow.PageFlowContext.getPolicy;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_OK;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;

import com.ail.core.BaseException;
import com.ail.core.JSONException;
import com.ail.core.PostconditionException;
import com.ail.core.RestfulServiceInvoker;
import com.ail.core.RestfulServiceReturn;
import com.ail.core.product.ProductServiceCommand;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.quotation.ApplyMTAQuotationService.ApplyMTAQuotationCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

/**
 * Service to apply a MTA quotation to the (master) policy being MTA'ed. This is a "pull" - it is invoked 
 * in the context of the policy being MTA'ed, it finds the SUBMITTED MTA quotation associated with the 
 * master policy by virtue of the policy links and updates the master wrt the quotation. 
 */
@ProductServiceCommand(serviceName = "ApplySubmittedMTAService", commandName = "ApplySubmittedMTA")
public class ApplySubmittedMTAService extends RestfulServiceInvoker {

    public static void invoke(ExecutePageActionArgument args) throws BaseException {
        new ApplySubmittedMTAService().invoke();
    }

    public RestfulServiceReturn service() throws PostconditionException, JSONException {
    	
    	try {
	    	Optional<Policy> mtaQuotation = fetchMTAQuotation();
	
	    	if (mtaQuotation.isPresent()) {
		    	ApplyMTAQuotationCommand arqc = getCoreProxy().newCommand(ApplyMTAQuotationCommand.class);
		    	arqc.setQuotationArg(mtaQuotation.get());
		    	arqc.invoke();
	
		    	return new Return(HTTP_OK, arqc.getPolicyRet());
	    	} else {
	    		String message = "A MTA_QUOTATION_FOR policy link to a quotation in SUBMITTED status could not be found for policy: " + PageFlowContext.getPolicy().getExternalSystemId();
	    		notify(message);
	    		return new Return(HTTP_BAD_REQUEST, message);
	    	}
    	} catch (Throwable e) {
    		notify(e);
            return new Return(HTTP_INTERNAL_ERROR, e.getMessage());
        }
    }

	protected void notify(String message) {
		// leave to subclasses to optionally implement
	}
    
	protected void notify(Throwable e) {
		// leave to subclasses to optionally implement
	}

	protected Optional<Policy> fetchMTAQuotation() {
    	for(PolicyLink pl: getPolicy().getPolicyLink()) {
    		if (pl.getLinkType() == MTA_QUOTATION_FOR) {
    			Policy linkTarget = (Policy)PageFlowContext.getCoreProxy().queryUnique("get.policy.by.systemId", pl.getTargetPolicyId());
    			if (linkTarget != null && linkTarget.getStatus() == SUBMITTED) {
    				return Optional.of(linkTarget);
    			}
    		}
    	}

    	return Optional.empty();
	}
    
	public static class Return extends RestfulServiceReturn {
        String message = null;
        MTA mta;

        public static class MTA {
            Long policyUID;
            String inceptonDate;
            String expiryDate;
            String insuredName;
            String policyNumber;

            public MTA(Policy policy) {
            	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
            	
            	this.policyUID = policy.getSystemId();
                this.inceptonDate = df.format(policy.getInceptionDate());
                this.expiryDate = df.format(policy.getExpiryDate());
                this.insuredName = policy.getClient().getLegalName();
                this.policyNumber = policy.getPolicyNumber();
            }
        }

        public Return(int status, Policy policy) {
            super(status);
            mta = new MTA(policy);
        }

        public Return(int status, String message) {
            super(status);
            this.message = message;
        }
    }
}