
/* Copyright Applied Industrial Logic Limited 2018. All rights reserved. */
import com.ail.core.Type;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyLinkType;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;
import com.ail.pageflow.util.Functions;

public class AddPolicyLinkService {
    public static void invoke(ExecutePageActionArgument args) {
        Type model = args.getModelArgRet();
        
        String typeAsString = (String)model.xpathGet("attribute[id='linkType']/value");
        if (typeAsString.equals("?")) {
            Functions.addError("badId", "required", (Type)model.xpathGet("attribute[id='linkType']"));
            typeAsString = null;
        }

        String targetAsString = (String)model.xpathGet("attribute[id='linkTarget']/value");
        if (com.ail.core.Functions.isEmpty(targetAsString)) {
            Functions.addError("badId", "required", (Type)model.xpathGet("attribute[id='linkTarget']"));
            targetAsString = null;
        }
        
        if (targetAsString != null && typeAsString != null) {
            PolicyLinkType type = PolicyLinkType.forName(typeAsString);
            Long target = fetchTargetPolicyId(targetAsString);
            
            if (target == null) {
            	Functions.addError("badId", "Target policy not found.", (Type)model.xpathGet("attribute[id='linkTarget']"));
            }
            else {
    	        PageFlowContext.getPolicy().getPolicyLink().add(new PolicyLink(type, target));
    	        
    	        model.xpathSet("attribute[id='linkType']/value", "?");
    	        model.xpathSet("attribute[id='linkTarget']/value", "");
            }
        }
        
        args.setModelArgRet(PageFlowContext.getPolicy());
    }
    
    private static Long fetchTargetPolicyId(String target) {
        Policy targetPolicy;

        if ((targetPolicy = (Policy)PageFlowContext.getCoreProxy().queryUnique("get.policy.by.quotationNumber", target)) != null) {
        	return targetPolicy.getSystemId();
        }
        
        if ((targetPolicy = (Policy)PageFlowContext.getCoreProxy().queryUnique("get.policy.by.policyNumber", target)) != null) {
        	return targetPolicy.getSystemId();
        }
        
        try {
        	return Long.parseLong(target);
        }
        catch(NumberFormatException e) {
        	return null;
        }
    }
}