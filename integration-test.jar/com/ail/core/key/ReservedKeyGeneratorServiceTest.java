package com.ail.core.key;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionException;
import com.ail.core.key.GenerateUniqueKeyService.GenerateUniqueKeyArgument;

public class ReservedKeyGeneratorServiceTest {

    private static final String TOKEN3 = "Token3";
    private static final String TOKEN2 = "Token2";
    private static final String TOKEN1 = "Token1";
    private static final String TEST_KEY_ID = "ID1";

    private CoreProxy core = null;

    private ReservedKeyGeneratorService sut;

    @Mock
    private GenerateUniqueKeyArgument args;

    public void setupTestData() {
        core.create(new ReservedKey(TEST_KEY_ID, TOKEN1));
        core.create(new ReservedKey(TEST_KEY_ID, TOKEN2));
        core.create(new ReservedKey(TEST_KEY_ID, TOKEN3));
    }

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        sut = new ReservedKeyGeneratorService();
        sut.setArgs(args);

        doReturn(TEST_KEY_ID).when(args).getKeyIdArg();

        CoreContext.initialise();
        core = new CoreProxy();
        core.openPersistenceSession();

        setupTestData();
    }

    @After
    public void teardown() {
        core.closePersistenceSession();
    }

    @Test
    public void test() throws PreconditionException, PostconditionException {
        sut.invoke();
        verify(args).setTokenRet(eq(TOKEN1));

        sut.invoke();
        verify(args).setTokenRet(eq(TOKEN2));

        sut.invoke();
        verify(args).setTokenRet(eq(TOKEN3));

        try {
            sut.invoke();
        } catch(PreconditionException e) {
            assertThat(e.getMessage(), is("Could not allocate token for: "+TEST_KEY_ID));
        }
    }
}
