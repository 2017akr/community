/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.financial.ledger;

import static javax.persistence.CascadeType.DETACH;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import org.hibernate.envers.Audited;

import com.ail.annotation.TypeDefinition;
import com.ail.core.Type;

@Entity
@Audited
@TypeDefinition
public class JournalGroup extends Type {

    private String purpose;

    private String description;

    @ManyToMany(cascade = { DETACH, MERGE, PERSIST, REFRESH })
    private Set<Journal> journal;

    public JournalGroup() {
    }

    public JournalGroup(String purpose, String description, Set<Journal> journal) {
        this.purpose = purpose;
        this.description = description;
        this.journal = journal;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String type) {
        this.purpose = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Journal> getJournal() {
        if (journal == null) {
            journal = new HashSet<>();
        }
        return journal;
    }

    public void setJournal(Set<Journal> journal) {
        this.journal = journal;
    }

    public void add(Journal journal) {
        getJournal().add(journal);
        journal.getJournalGroup().add(this);
    }

    public void remove(Journal journal) {
        getJournal().remove(journal);
        journal.getJournalGroup().remove(this);
    }

    public void removeAll(Collection<Journal> journals) {
        getJournal().removeAll(journals);
        journals.forEach(jou -> jou.getJournalGroup().remove(this));
    }

    public void addAll(Collection<Journal> journals) {
        getJournal().addAll(journals);
        journals.forEach(jou -> jou.getJournalGroup().add(this));
    }

    public void clear() {
        removeAll(journal);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.typeHashCode();
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((journal == null) ? 0 : journal.hashCode());
        result = prime * result + ((purpose == null) ? 0 : purpose.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JournalGroup other = (JournalGroup) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        if (journal == null) {
            if (other.journal != null)
                return false;
        } else if (!journal.equals(other.journal))
            return false;
        if (purpose == null) {
            if (other.purpose != null)
                return false;
        } else if (!purpose.equals(other.purpose))
            return false;
        return super.typeEquals(obj);
    }

    @Override
    public String toString() {
        return "JournalGroup [getSystemId()=" + getSystemId() + ", purpose=" + purpose + ", description=" + description + ", journal=" + journal + "]";
    }

}
