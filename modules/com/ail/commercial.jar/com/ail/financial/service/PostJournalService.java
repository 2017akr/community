/* Copyright Applied Industrial Logic Limited 2017. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.financial.service;

import static com.ail.core.CoreContext.getCoreProxy;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.financial.ledger.Journal;
import com.ail.financial.ledger.JournalLine;
import com.ail.financial.service.PostJournalService.PostJournalArgument;

/**
 */
@ServiceImplementation
public class PostJournalService extends Service<PostJournalArgument> {

    @Override
    public void invoke() throws BaseException {
        if (args.getJournalArgRet() == null) {
            throw new PreconditionException("args.getJournalArgRet() == null");
        }

        Journal journal = args.getJournalArgRet();

        new LinkJournalToAccountingPeriod(journal).invoke();

        journal = getCoreProxy().create(journal);

        for(JournalLine line: journal.getJournalLine()) {
            getCoreProxy().create(line);
        }
    }

    @ServiceCommand(defaultServiceClass = PostJournalService.class)
    public interface PostJournalCommand extends Command, PostJournalArgument {
    }

    @ServiceArgument
    public interface PostJournalArgument extends Argument {
        void setJournalArgRet(Journal journalArgRet);

        Journal getJournalArgRet();
    }
}
