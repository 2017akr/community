/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.core.key;

import static java.lang.String.format;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.ail.annotation.ServiceImplementation;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.key.GenerateUniqueKeyService.GenerateUniqueKeyArgument;
import com.ail.core.persistence.hibernate.HibernateSessionBuilder;

/**
 * An implementation of GenerateUniqueKey generating keys from a predefined list
 * of valid values. A a supplier provides a set of predefined tokens which are
 * loaded into the reserved key table. This service will take a token from the
 * table each time it is called. Once read, the token is deleted from the table.
 */
@ServiceImplementation
public class ReservedKeyGeneratorService extends Service<GenerateUniqueKeyArgument> {

    @Override
    public void invoke() throws PreconditionException, PostconditionException {
        String keyIdParamName = "KeyGenerators." + args.getKeyIdArg() + ".ID";

        String generatorId = args.getKeyIdArg();

        if (generatorId == null) {
            throw new PreconditionException(keyIdParamName + " not defined.");
        }

        ReservedKey key = fetchKey(generatorId);

        args.setKeyRet(key.getSystemId());
        args.setTokenRet(key.getValue());
    }

    private ReservedKey fetchKey(String generatorId) throws PostconditionException, PreconditionException {
        try {
            Session session = HibernateSessionBuilder.getSessionFactory().getCurrentSession();

            Query query = session.getNamedQuery("get.next.key.by.id");
            query.setParameter(0, generatorId);
            query.setMaxResults(1);

            @SuppressWarnings("unchecked")
            List<ReservedKey> keys = query.list();
            if (keys.size() == 1) {
                ReservedKey reservedKey = keys.get(0);
                session.delete(reservedKey);
                return reservedKey;
            } else {
               throw new PreconditionException(format("Could not allocate token for: %s", generatorId));
            }
        } catch (PreconditionException e) {
            throw e;
        } catch (Throwable e) {
            throw new PostconditionException(format("Reserved key generation failed for: %s", generatorId), e);
        }
    }
}