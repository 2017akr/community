/* Copyright Applied Industrial Logic Limited 2015. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.core.persistence.hibernate;

import static com.ail.core.CoreContext.getCoreProxy;

import java.util.Optional;

import org.hibernate.StaleObjectStateException;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.Functions;
import com.ail.core.Type;
import com.ail.core.audit.FetchRevisionsService.FetchRevisionsCommand;
import com.ail.core.audit.Revision;

class DetailedStaleObjectStateMessageBuilder {
    private StaleObjectStateException sose = null;

    DetailedStaleObjectStateMessageBuilder(StaleObjectStateException sose) {
        this.sose = sose;
    }

    String build() {
        try {
            return new HibernateRunInTransaction<String>() {
                @Override
                public String run() throws Throwable {
                    return buildDetailedMessage(sose);
                }
            }.invoke().result();
        } catch (Throwable e) {
            return "Row was updated or deleted concurrently, the other transaction cannot be identified. Caused by: "+sose.getMessage();
        }
    }

    @SuppressWarnings("unchecked")
    private String buildDetailedMessage(StaleObjectStateException sose) {
        String entityType = sose.getEntityName();
        Long entityId = (sose.getIdentifier() instanceof Long) ? (Long) sose.getIdentifier() : null;

        if (entityType != null || entityId != null) {
            try {
                CoreContext.setCoreProxy(new CoreProxy());

                Class<? extends Type> entityClass = (Class<? extends Type>) Functions.classForName(entityType);

                Optional<Revision> latestRevision = fetchLatestRevisionFor(entityClass, entityId);

                if (latestRevision.isPresent()) {
                    return "Row was updated or deleted concurrently by: "+latestRevision.get().getRevisionDetails().getServiceRequestRecord().getCommand();
                }
            } catch (ClassNotFoundException | BaseException | InstantiationException | IllegalAccessException e) {
                // ignore - fall through to default return.
            }
            finally {
                CoreContext.setCoreProxy(null);
            }
        }

        return sose.getMessage();
    }

    private Optional<Revision> fetchLatestRevisionFor(Class<? extends Type> entityClass, Long entityId) throws BaseException, InstantiationException, IllegalAccessException {
        FetchRevisionsCommand frnc = getCoreProxy().newCommand(FetchRevisionsCommand.class);

        Type typeStub = entityClass.newInstance();
        typeStub.setSystemId(entityId);

        frnc.setTypeArg(typeStub);

        frnc.invoke();

        if (frnc.getRevisionsRet().size() > 0) {
            Revision latestRevision = frnc.getRevisionsRet().get(frnc.getRevisionsRet().size() - 1);
            return Optional.of(latestRevision);
        }

        return Optional.empty();
    }
}
