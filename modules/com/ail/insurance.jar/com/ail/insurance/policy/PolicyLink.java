package com.ail.insurance.policy;

import static javax.persistence.EnumType.STRING;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;

import org.hibernate.envers.Audited;

import com.ail.annotation.TypeDefinition;
import com.ail.core.Type;

/* Copyright Applied Industrial Logic Limited 2015. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/**
 * <p>
 * A PolicyLink describes a relationship from a Policy to another Policy. A
 * Policy object carries a collection (set) of such relationships which allow it
 * to define, for example, the policy that it renewed from, the quotation that
 * it was disaggregated from.
 * </p>
 * <p>
 * A PolicyLink may refer to either the current version of another policy, or to
 * a history audit record of it. To refer to an audit record either the
 * {@link #revisionId} or the {@link #revisionDate} must be populated. Using the
 * {@link #revisionDate} indicates that whichever revision of the policy that
 * was in effect at that data is being linked to; {@link #revisionId} links to a
 * specific revision.
 * </p>
 * <p>
 * Where both {@link #revisionId} and {@link #revisionDate} are specified,
 * {@link #revisionId} takes precedence.
 * </p>
 */
@Audited
@Entity
@TypeDefinition
public class PolicyLink extends Type {
    @Enumerated(STRING)
    private PolicyLinkType linkType;

    @Column(name="targetPolicyUIDpol")
    private Long targetPolicyId;

    private Long revisionId;

    private Date revisionDate;

    /** No args constructor for Hibernate/Castor etc.
     * @deprecated For framework use only. Use {@link #PolicyLink(PolicyLinkType, Long)} or {@link #PolicyLink(PolicyLinkType, Long, Long)} instead
     */
    @Deprecated
    public PolicyLink() {
    }

    public PolicyLink(PolicyLinkType type, Long targetPolicyId, Long revisionId, Date revisionDate) {
        this.linkType = type;
        this.targetPolicyId = targetPolicyId;
        this.revisionId = revisionId;
        this.revisionDate = revisionDate;
    }

    public PolicyLink(PolicyLinkType type, Long targetPolicyId, Long revisionId) {
        this(type, targetPolicyId, revisionId, null);
    }

    public PolicyLink(PolicyLinkType type, Long targetPolicyId, Date revisionDate) {
        this(type, targetPolicyId, null, revisionDate);
    }

    public PolicyLink(PolicyLinkType type, Long targetPolicyId) {
        this(type, targetPolicyId, null, null);
    }

    public PolicyLinkType getLinkType() {
        return linkType;
    }

    public void setLinkType(PolicyLinkType linkType) {
        this.linkType = linkType;
    }


    /** @deprecated use {@link #getTargetPolicyId()} instead. */
    @Deprecated
    public Long getPolicySystemId() {
        return targetPolicyId;
    }

    /** @deprecated use {@link #setTargetPolicyId(Long)} instead. */
    @Deprecated
    public void setPolicySystemId(Long policySystemId) {
        this.targetPolicyId = policySystemId;
    }

    public Long getTargetPolicyId() {
        return targetPolicyId;
    }

    public void setTargetPolicyId(Long targetPolicyId) {
        this.targetPolicyId = targetPolicyId;
    }

    public Long getRevisionId() {
        return revisionId;
    }

    public void setRevisionId(Long revisionId) {
        this.revisionId = revisionId;
    }

    public Date getRevisionDate() {
        return revisionDate;
    }

    public void setRevisionDate(Date revisionDate) {
        this.revisionDate = revisionDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.typeHashCode();
        result = prime * result + ((linkType == null) ? 0 : linkType.hashCode());
        result = prime * result + ((targetPolicyId == null) ? 0 : targetPolicyId.hashCode());
        result = prime * result + ((revisionId == null) ? 0 : revisionId.hashCode());
        result = prime * result + ((revisionDate == null) ? 0 : revisionDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PolicyLink other = (PolicyLink) obj;
        if (linkType != other.linkType)
            return false;
        if (targetPolicyId == null) {
            if (other.targetPolicyId != null)
                return false;
        } else if (!targetPolicyId.equals(other.targetPolicyId))
            return false;
        if (revisionId == null) {
            if (other.revisionId != null)
                return false;
        } else if (!revisionId.equals(other.revisionId))
            return false;
        if (revisionDate == null) {
            if (other.revisionDate != null)
                return false;
        } else if (!revisionDate.equals(other.revisionDate))
            return false;
        return super.typeEquals(obj);
    }

    @Override
    public String toString() {
        return "PolicyLink [getSystemId()=" + getSystemId() + ", linkType=" + linkType + ", policySystemId=" + targetPolicyId + ", revisionId=" + revisionId + ", revisionDate=" + revisionDate + "]";
    }

}
