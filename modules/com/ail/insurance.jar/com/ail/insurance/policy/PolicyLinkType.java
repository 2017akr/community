/* Copyright Applied Industrial Logic Limited 2015. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.insurance.policy;

import java.util.Arrays;
import java.util.List;

import com.ail.core.Functions;
import com.ail.core.TypeEnum;

public enum PolicyLinkType implements TypeEnum {

    DISAGGREGATED_FROM("i18n_policy_link_type_disaggregated_from"),
    DISAGGREGATED_TO("i18n_policy_link_type_disaggregated_to"),

    /* @deprecated */
    RENEWAL_OF("Renewal Of"),

    MTA_QUOTATION_FROM("i18n_policy_link_type_mta_quotation_from"),
    MTA_QUOTATION_FOR("i18n_policy_link_type_mta_quotation_for"),

    MTA_FROM("i18n_policy_link_type_mta_from"),
    MTA_FOR("i18n_policy_link_type_mta_for"),

    RENEWAL_QUOTATION_FROM("i18n_policy_link_type_renewal_quotation_from"),
    RENEWAL_QUOTATION_FOR("i18n_policy_link_type_renewal_quotation_for"),

    RENEWAL_FROM("i18n_policy_link_type_renewal_from"),
    RENEWAL_FOR("i18n_policy_link_type_renewal_for"),

    CANCELLATION_QUOTATION_FROM("i18n_policy_link_type_cancellation_quotation_from"),
    CANCELLATION_QUOTATION_FOR("i18n_policy_link_type_cancellation_quotation_for"),

    CANCELLATION_FROM("i18n_policy_link_type_cancellation_from"),
    CANCELLATION_FOR("i18n_policy_link_type_cancellation_for");

    public static final List<PolicyLinkType> PARENT_LINK_TYPES = Arrays.asList(new PolicyLinkType[] {MTA_QUOTATION_FROM, MTA_FROM, RENEWAL_QUOTATION_FROM, RENEWAL_FROM, CANCELLATION_QUOTATION_FROM, CANCELLATION_FROM});

    private final String longName;

    PolicyLinkType(String longName) {
        this.longName=longName;
    }

    public String valuesAsCsv() {
        return Functions.arrayAsCsv(values());
    }

    @Override
    public String longName() {
        return longName;
    }

    /**
     * This method is similar to the valueOf() method offered by Java's Enum type, but
     * in this case it will match either the Enum's name or the longName.
     * @param name The name to lookup
     * @return The matching Enum, or IllegalArgumentException if there isn't a match.
     */
    public static PolicyLinkType forName(String name) {
        return (PolicyLinkType)Functions.enumForName(name, values());
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public String getLongName() {
        return longName;
    }

    @Override
    public int getOrdinal() {
        return ordinal();
    }
}
