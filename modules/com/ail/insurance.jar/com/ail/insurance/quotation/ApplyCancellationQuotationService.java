/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.insurance.quotation;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.insurance.policy.PolicyLinkType.CANCELLATION_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLIED;
import static com.ail.insurance.policy.PolicyStatus.CANCELLED;
import static com.ail.insurance.policy.PolicyStatus.CANCELLED_FROM_INCEPTION;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.CloneError;
import com.ail.core.PreconditionError;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.core.document.Document;
import com.ail.financial.PaymentRecord;
import com.ail.insurance.policy.AssessmentSheet;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.Section;
import com.ail.insurance.quotation.ApplyCancellationQuotationService.ApplyCancellationQuotationArgument;
import com.ail.insurance.quotation.FinaliseCancellationService.FinaliseCancellationCommand;

/**
 */
@ServiceImplementation
public class ApplyCancellationQuotationService extends Service<ApplyCancellationQuotationArgument> {
    private static final long serialVersionUID = 3819563603833694389L;

    @ServiceArgument
    public interface ApplyCancellationQuotationArgument extends Argument {
        void setQuotationArg(Policy quotationArg);

        Policy getQuotationArg();

        void setPolicyRet(Policy policyRet);

        Policy getPolicyRet();
    }

    @ServiceCommand(defaultServiceClass = ApplyCancellationQuotationService.class)
    public interface ApplyCancellationQuotationCommand extends Command, ApplyCancellationQuotationArgument {
    }

    @Override
    public void invoke() throws BaseException {
        if (args.getQuotationArg() == null) {
            throw new PreconditionException("args.getQuotationArg() == null");
        }

        if (args.getQuotationArg().getStatus() != SUBMITTED) {
            throw new PreconditionException("args.getQuotationArg().getStatus() != SUBMITTED");
        }

        List<PolicyLink> policyLinksForQuotationFrom = fetchPolicyLinksForQuotationFrom();

        if (policyLinksForQuotationFrom.size() != 1) {
            throw new PreconditionException("fetchPolicyLinksForQuotationFrom.size() != 1");
        }

        Policy masterPolicy = fetchMasterPolicy(policyLinksForQuotationFrom.get(0).getTargetPolicyId());

        if (masterPolicy == null) {
            throw new PreconditionException("masterPolicy == null");
        }

        if (masterPolicy.getStatus() != ON_RISK) {
            throw new PreconditionException("masterPolicy.getStatus() != ON_RISK");
        }

        Policy quotationPolicy = args.getQuotationArg();

        copyInforceAndExpiryDates(quotationPolicy, masterPolicy);

        copyQuotationDocument(quotationPolicy, masterPolicy);

        copyAssessmentSheets(quotationPolicy, masterPolicy);

        copyPaymentSchedule(quotationPolicy, masterPolicy);

        copyPaymentHistory(quotationPolicy, masterPolicy);

        finaliseCancellation(quotationPolicy, masterPolicy);

        quotationPolicy.setStatus(APPLIED);

        masterPolicy.setStatus(isCancelFromInception() ? CANCELLED_FROM_INCEPTION : CANCELLED);

        args.setPolicyRet(masterPolicy);
    }

    private boolean isCancelFromInception() {
        return args.getQuotationArg().getAssessmentSheet().findLineById(CANCELLED_FROM_INCEPTION.toString()) != null;
    }

    private void copyPaymentHistory(Policy quotationPolicy, Policy parentPolicy) {
        quotationPolicy.getPaymentHistory().forEach(ph -> {
            try {
                parentPolicy.getPaymentHistory().add((PaymentRecord)ph.clone());
            } catch (CloneNotSupportedException e) {
                throw new PreconditionError(String.format("Failed to clone payment history record: %d from quoation: %d into parentPolicy: %d", ph.getSystemId(), quotationPolicy.getSystemId(), parentPolicy.getSystemId()));
            }
        });
    }

    private void finaliseCancellation(Policy quotationPolicy, Policy masterPolicy) throws BaseException {
        FinaliseCancellationCommand fcc = getCoreProxy().newCommand("FinaliseCancellation", FinaliseCancellationCommand.class);
        fcc.setPolicyArg(masterPolicy);
        fcc.setQuotationArg(quotationPolicy);
        fcc.invoke();
    }

    private void copyPaymentSchedule(Policy quotationPolicy, Policy masterPolicy) {
        masterPolicy.setPaymentDetails(quotationPolicy.getPaymentDetails());
    }

    private void copyAssessmentSheets(Policy quotationPolicy, Policy masterPolicy) {
        masterPolicy.setAssessmentSheetList(cloneAssessmentSheets(quotationPolicy.getAssessmentSheetList()));

        quotationPolicy.getSection().
                        stream().
                        forEach(sourceSecion -> {
                            Section targetSection;
                            if ((targetSection = findMatchingSection(sourceSecion, masterPolicy)) != null) {
                                targetSection.setAssessmentSheetList(cloneAssessmentSheets(sourceSecion.getAssessmentSheetList()));
                            }
                        });
    }

    Map<String, AssessmentSheet> cloneAssessmentSheets(Map<String, AssessmentSheet> source) throws CloneError {
        Map<String, AssessmentSheet> target = new HashMap<>();

        try {
            for(String key: source.keySet()) {
                AssessmentSheet clone = (AssessmentSheet)source.get(key).clone();
                clone.markAsNotPersisted();
                target.put(key, clone);
            }
        } catch (CloneNotSupportedException e) {
            throw new CloneError("Failed to clone assessmentSheet: " + source, e);
        }

        return target;
    }

    private Section findMatchingSection(Section target, Policy policy) {
        Section candidate = policy.getSectionById(target.getId());

        if (candidate != null && candidate.getSectionTypeId().equals(target.getSectionTypeId())) {
            return candidate;
        }
        else {
            return null;
        }
    }

    private void copyQuotationDocument(Policy quotationPolicy, Policy masterPolicy) {
        Document quotationDocument;

        if ((quotationDocument = quotationPolicy.retrieveQuotationDocument()) != null) {
            masterPolicy.attachQuotationDocument(quotationDocument.getDocumentContent().getContent());
        }
    }

    private void copyInforceAndExpiryDates(Policy quotationPolicy, Policy masterPolicy) {
        masterPolicy.setInforceDate(quotationPolicy.getInceptionDate());
        masterPolicy.setExpiryDate(quotationPolicy.getExpiryDate());
    }

    private Policy fetchMasterPolicy(long systemId) {
        return (Policy)getCoreProxy().queryUnique("get.policy.by.systemId", systemId);
    }

    private List<PolicyLink> fetchPolicyLinksForQuotationFrom() {
        return args.getQuotationArg().
                    getPolicyLink().
                    stream().
                    filter(pl -> pl.getLinkType() == CANCELLATION_QUOTATION_FROM).
                    collect(Collectors.toList());
    }
}
