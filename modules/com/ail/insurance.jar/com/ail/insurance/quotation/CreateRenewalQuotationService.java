/* Copyright Applied Industrial Logic Limited 2018. All rights Reserved */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

package com.ail.insurance.quotation;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.APPLIED;
import static com.ail.insurance.policy.PolicyStatus.NOT_TAKEN_UP;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.quotation.CreateRenewalQuotationService.CreateRenewalQuotationArgument;
import com.ail.insurance.quotation.InitialiseRenewalService.InitialiseRenewalCommand;
import com.ail.insurance.quotation.PrepareRequoteService.PrepareRequoteCommand;

/**
 * This service will:<ol>
 * <li>Confirm that the renewing policy is {@link PolicyStatus#ON_RISK ON_RISK} and error if it is not.</li>
 * <li>Search the renewing policy’s PolicyLinks for old renewal quotations. Any which were not Applied will have their status moved to {@link PolicyStatus#NOT_TAKEN_UP NOT_TAKEN_UP}.</li>
 * <li>Invoke the {@link PrepareRequoteService} service to create a quotation policy.</li>
 * <li>Create {@link PolicyLink PolicyLinks} to link the Policy to the quotation (linkType = {@link PolicyLink#RENEWAL_QUOTATION_FOR RENEWAL_QUOTATION_FOR}), and link the quotation to the Policy (linkType = {@link PolicyLink#RENEWAL_QUOTATION_FROM RENEWAL_QUOTATION_FROM})
 * <li>Set the quotation policy's status to {@link PolicyStatus.APPLICATION APPLICATION}
 * <li>Invoke the product specific {@link InitialiseRenewalService} service.</li>
 * </ol>
 */
@ServiceImplementation
public class CreateRenewalQuotationService extends Service<CreateRenewalQuotationArgument> {
    private static final long serialVersionUID = 3819563603833694389L;

    @ServiceArgument
    public interface CreateRenewalQuotationArgument extends Argument {
        void setPolicyArg(Policy policy);

        Policy getPolicyArg();

        void setRenewalRet(Policy renewalRet);

        Policy getRenewalRet();
    }

    @ServiceCommand(defaultServiceClass = CreateRenewalQuotationService.class)
    public interface CreateRenewalQuotationCommand extends Command, CreateRenewalQuotationArgument {
    }

    @Override
    public void invoke() throws BaseException {
        if (args.getPolicyArg() == null) {
            throw new PreconditionException("args.getPolicyArg() == null");
        }

        if (args.getPolicyArg().getStatus() != ON_RISK) {
            throw new PreconditionException("args.getPolicyArg().getStatus() != ON_RISK");
        }

        markExistingRenewalQuotationsAsNotTakenUp();

        createRenewalQuotation();

        getCore().flush(); // flush the renewal so we get IDs etc. populated.

        linkRenewalQuotationToPolicy();

        setRenewalStatusToApplication();

        initialiseRenewalQuotation();

        if (args.getRenewalRet() == null) {
            throw new PostconditionException("args.getRenewalRet() == null");
        }
    }

    private void setRenewalStatusToApplication() {
        args.getRenewalRet().setStatus(APPLICATION);
    }

    private void initialiseRenewalQuotation() throws BaseException {
        InitialiseRenewalCommand irc = getCoreProxy().newCommand("InitialiseRenewal", InitialiseRenewalCommand.class);
        irc.setRenewingPolicyArg(args.getPolicyArg());
        irc.setRenewalQuotationArg(args.getRenewalRet());
        irc.invoke();
    }

    private void linkRenewalQuotationToPolicy() {
        args.getPolicyArg().getPolicyLink().add(new PolicyLink(RENEWAL_QUOTATION_FOR, args.getRenewalRet().getSystemId()));
        args.getRenewalRet().getPolicyLink().add(new PolicyLink(RENEWAL_QUOTATION_FROM, args.getPolicyArg().getSystemId()));
    }

    private void createRenewalQuotation() throws BaseException {
        PrepareRequoteCommand prc = getCoreProxy().newCommand(PrepareRequoteCommand.class);
        prc.setPolicyArg(args.getPolicyArg());
        prc.setSuppressDocumentCopyArg(true);
        prc.invoke();

        args.setRenewalRet(getCoreProxy().create(prc.getRequoteRet()));
    }

    /**
     * Search for all quotations that were created as renewals for this policy in the past.
     * Any that are found will which have not been applied are marked as NOT_TAKEN_UP.
     */
    private void markExistingRenewalQuotationsAsNotTakenUp() {
        args.getPolicyArg().getPolicyLink().
                stream().
                filter(pl -> pl.getLinkType() == RENEWAL_QUOTATION_FOR).
                forEach(pl -> {
                    Policy oldRenewal = (Policy)getCoreProxy().queryUnique("get.policy.by.systemId", pl.getTargetPolicyId());
                    if (oldRenewal.getStatus() != APPLIED) {
                        oldRenewal.setStatus(NOT_TAKEN_UP);
                    }
                });
    }
}
