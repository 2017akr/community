/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.ail.pageflow.render;

import static com.ail.core.CoreContext.getCoreProxy;
import static com.ail.core.Functions.isEmpty;
import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;

import com.ail.annotation.ServiceArgument;
import com.ail.annotation.ServiceCommand;
import com.ail.annotation.ServiceImplementation;
import com.ail.core.BaseException;
import com.ail.core.PreconditionException;
import com.ail.core.Service;
import com.ail.core.Type;
import com.ail.core.command.Argument;
import com.ail.core.command.Command;
import com.ail.insurance.policy.Policy;
import com.ail.pageflow.render.BulkNotifyEmailService.BulkNotifyEmailArgument;
import com.ail.pageflow.render.NotifyEmailService.NotifyEmailCommand;
import com.ail.party.Party;

/**
 * A simple but comprehensive generic email service.
 * Body text can be expressed with a template or by simple text.
 * Attachments are added using document retrieval commands.
 */
@ServiceImplementation
public class BulkNotifyEmailService extends Service<BulkNotifyEmailArgument> {

    @Override
    public void invoke() throws BaseException {

        if (isEmpty(args.getEntitySelectorQueryArg())) {
            throw new PreconditionException("isEmpty(args.getEntitySelectorQueryArg())");
        }

        if (isEmpty(args.getRecipientSelectorArg())) {
            throw new PreconditionException("isEmpty(args.getRecipientSelectorArg())");
        }

        if (isEmpty(args.getSubjectArg())) {
            throw new PreconditionException("isEmpty(args.getSubjectArg())");
        }

        if (isEmpty(args.getFromArg())) {
            throw new PreconditionException("isEmpty(args.getFromArg())");
        }

        if (isEmpty(args.getTemplateNameArg()) && isEmpty(args.getTextArg())) {
            throw new PreconditionException("isEmpty(args.getTemplateNameArg()) && isEmpty(args.getTextArg())");
        }

        if (!isEmpty(args.getTemplateNameArg()) && !isEmpty(args.getTextArg())) {
            throw new PreconditionException("!isEmpty(args.getTemplateNameArg()) && !isEmpty(args.getTextArg())");
        }

        args.setResultsRet(new ArrayList<>());

        for(Object obj: getCoreProxy().query(args.getEntitySelectorQueryArg())) {
            if (!(obj instanceof Type)) {
                throw new PreconditionException(format("PolicySelectorQueryArg return entity of type: '%s'. Only subtypes of %s are supported.", obj.getClass().getName(), Type.class.getName()));
            }

            Type type = (Type)obj;

            Party recipient = type.xpathGet(args.getRecipientSelectorArg(), null, Party.class);

            if (recipient == null) {
                args.getResultsRet().add(format("FAILURE: For %s(id=%s) recipient selector (%s) resolved to null value.", type.getClass().getSimpleName(), type.getExternalSystemId(), args.getRecipientSelectorArg()));
            }
            else if (isEmpty(recipient.getEmailAddress())) {
                args.getResultsRet().add(format("FAILURE: For %s(id=%s) recipient (id=%s) has an empty email adrress.", type.getClass().getSimpleName(), type.getExternalSystemId(), recipient.getExternalSystemId()));
            }
            else {
                NotifyEmailCommand nec = getCoreProxy().newCommand(NotifyEmailCommand.class);
                nec.setFromArg(args.getFromArg());
                nec.setPolicyArg((Policy)type); // TODO We're forced to cast to Policy just because NotifyEmailCommand only understands Policy (see OU-1193).
                nec.setSubjectArg(args.getSubjectArg());
                nec.setTemplateNameArg(args.getTemplateNameArg());
                nec.setTextArg(args.getTextArg());
                nec.setToArg(recipient.getEmailAddress());
                nec.invoke();
                args.getResultsRet().add(format("SUCCESS: Email sent to %s for %s(id=%s)", recipient.getEmailAddress(), type.getClass().getSimpleName(), type.getExternalSystemId()));
            }
        }
    }


    @ServiceArgument
    public interface BulkNotifyEmailArgument extends Argument {

        String getEntitySelectorQueryArg();

        /**
         * An HQL query selecting the entities for which emails are to be sent
         * @param queryArg
         */
        void setEntitySelectorQueryArg(String entitySelectorQueryArg);

        String getRecipientSelectorArg();

        /**
         * An xpath expression to select the party to whom the email should be sent using Policy as a root for evaluation.
         * @param toSelectorArg
         */
        void setRecipientSelectorArg(String recipientSelectorArg);

        String getSubjectArg();

        /**
         * Email subject line
         * @param subjectArg
         */
        void setSubjectArg(String subjectArg);

        String getFromArg();

        /**
         * From email address - defaults to mail properties if not set
         * @param from
         */
        void setFromArg(String from);

        String getTextArg();

        /**
         * A simple way to set body summary text without using a template
         * @param text
         */
        void setTextArg(String text);

        String getTemplateNameArg();

        /**
         * Set main body summary template name
         * @param templateNameArg
         */
        void setTemplateNameArg(String templateNameArg);

        List<String> getResultsRet();

        /**
         * A summary of the results
         * @param resultsRet
         */
        void setResultsRet(List<String> resultsRet);

    }

    @ServiceCommand(defaultServiceClass=BulkNotifyEmailService.class)
    public interface BulkNotifyEmailCommand extends Command, BulkNotifyEmailArgument {
    }

}
