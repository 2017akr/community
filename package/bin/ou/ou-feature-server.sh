#!/bin/bash

function help {
    echo "create"
	echo "create-if-not-present"
    echo "delete"
	echo "update"
    echo "warm-up"
    echo "exists"
    echo "domain-name"
    echo "status"
}

function create-if-not-present {
	if [ "$(ou.sh feature-server exists -branch=$(branch-name))" == "no" ]; then 
		create
		ou.sh slack post -message="Feature server created: https://$(domain-name)/web/ou (allow a few minutes for DNS for propagate)"
	else
		info "Feature server already exists. Nothing to do."  
	fi
	return 0
}

function upload {
	INITIAL_DIR=$PWD
	cd $(product-dir)
    ant -f Build.xml -Dou.home=$OU_HOME -Dwebdav.host=$(domain-name) -Dwebdav.port=443 -Dwebdav.protocol=https -Dwebdav.username=$(username) -Dwebdav.password=$(password) -Duse.os.get=yes
    cd $INITIAL_DIR
}

function create {
    load-cloudflare-env
    load-feature-server-aws-env

    info "Creating instance..."
    INSTANCE_ID=$(aws ec2 run-instances --region $AWS_REGION --launch-template LaunchTemplateId=lt-0132e09fb719c515d --output text --query 'Instances[0].InstanceId')
    
    info "Setting instance name to: $(instance-name)..."
    aws ec2 create-tags --region $AWS_REGION --resources $INSTANCE_ID --tag Key=Name,Value="$(instance-name)" Key=Class,Value=Development

    info "Waiting for instance to start..."
    while [ $(aws ec2 describe-instance-status --region $AWS_REGION --instance-ids $INSTANCE_ID --output text --query 'InstanceStatuses[0].InstanceStatus.Status') != "ok" ]; do sleep 5; done
    
    info "Fetching instance IP addresses..."
    PUBLIC_IP=$(aws ec2 describe-instances --region $AWS_REGION --instance-ids $INSTANCE_ID --output text --query "Reservations[0].Instances[0].PublicIpAddress")
    PRIVATE_IP=$(aws ec2 describe-instances --region $AWS_REGION --instance-ids $INSTANCE_ID --output text --query "Reservations[0].Instances[0].PrivateIpAddress")
    info "Public PI address: $PUBLIC_IP, private IP address: $PRIVATE_IP."
    
    info "Setting up Cloudflare DNS record..."
    DNS_RECORD=$(
        curl -X POST "https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONE_ID/dns_records" -s \
             -H "X-Auth-Email: $CLOUDFLARE_USER" \
             -H "X-Auth-Key: $CLOUDFLARE_API_KEY" \
             -H "Content-Type: application/json" \
             --data '{"type":"A","name":"'$(branch-host-name)'","content":"'$PUBLIC_IP'","ttl":120,"priority":10,"proxied":false}')
    SUCCESS=$(echo $DNS_RECORD | json-query "success")
    [ "$SUCCESS" != "true" ] && error "DNS record setup failed."
    info "DNS record setup for $(domain-name) successful."
     
	info "Adding feature server to hosts file..."
	echo "$PUBLIC_IP $(domain-name)" >> /etc/hosts

    LIFERAY_HOME="/opt/openunderwriter/openunderwriter-community/liferay-portal-6.2-ce-ga6"
    HTTPD_HOME="/etc/httpd/conf.d/"
    BIRT_WAR="$LIFERAY_HOME/jboss-7.1.1/standalone/deployments/birt.war"
    OU_LOG="$LIFERAY_HOME/jboss-7.1.1/standalone/log/server.log"
         
    info "Fixing up OU instance's domain name settings in: hosts, apache, birt, and liferay; and starting OU..."
    ssh -o "StrictHostKeyChecking no" ec2-user@$PUBLIC_IP \
        sudo 'echo "127.0.0.1 ip-'${PRIVATE_IP//\./-}'" | sudo tee -a /etc/hosts > /dev/null'\; \
        sudo sed -i "s:feature:$(branch-host-name):" $LIFERAY_HOME/portal-ext.properties\; \
        sudo sed -i "s:feature:$(branch-host-name):" $HTTPD_HOME/openunderwriter.conf\; \
        sudo sed -i "s:feature:$(branch-host-name):" $HTTPD_HOME/openunderwriter-ssl.conf\; \
        sudo service httpd restart \> /dev/null \; \
        sudo jar -xf $BIRT_WAR WEB-INF/viewer.properties\; \
        sudo sed -i 's@#base_url=http://127.0.0.1:8080@base_url=http://'$(branch-host-name)'@' WEB-INF/viewer.properties\; \
        sudo jar -uf $BIRT_WAR WEB-INF/viewer.properties\; \
        sudo chown openunderwriter.openunderwriter $BIRT_WAR\; \
        sudo rm -rf WEB-INF\; \
        sudo service openunderwriter start \> /dev/null
            
    info "Waiting for OU startup to complete..."
    ssh -o "StrictHostKeyChecking no" ec2-user@$PUBLIC_IP 'sleep 20; while [ -z "$(tail -10 '$OU_LOG' 2> /dev/null | grep JBAS018559)" ]; do echo -n "."; sleep 10; done'
    
	info "Waiting for first response from OU..."
	while [ -z "$(curl -s https://$(domain-name) 2>/dev/null | grep 'Liferay')" ]; do sleep 10; done
	
    info "Feature server creation successful."
}

function exists {
    load-feature-server-aws-env
    
    [ ! -z "$(aws ec2 describe-instances --region $AWS_REGION --filters "Name=tag:Name,Values=$(instance-name)" --output text --query 'Reservations[*].Instances[*].InstanceId')" ] && echo "yes" || echo "no"
}

function status {
    load-feature-server-aws-env

    echo $(aws ec2 describe-instances --region $AWS_REGION --filters "Name=tag:Name,Values=$(instance-name)" --output text --query 'Reservations[*].Instances[*].State.Name')
}

function delete {
    load-cloudflare-env
    load-feature-server-aws-env

    info "Fetching Cloudflare DNS ID for $(domain-name)..."
    DNS_RECORD=$( 
        curl -X GET "https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONE_ID/dns_records?name=$(domain-name)&match=all" -s \
             -H "X-Auth-Email: $CLOUDFLARE_USER" \
             -H "X-Auth-Key: $CLOUDFLARE_API_KEY" \
             -H "Content-Type: application/json")
    DNS_IDENTIFIER=$(echo $DNS_RECORD | json-query "result[0].id") 
        
    [ -z "$DNS_IDENTIFIER" ] && error "Failed to find Cloudflare DNS record for $(domain-name)"

    info "Deleting DNS record..."
    SUCCESS=$(
        curl -X DELETE "https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONE_ID/dns_records/$DNS_IDENTIFIER" -s \
             -H "X-Auth-Email: $CLOUDFLARE_USER" \
             -H "X-Auth-Key: $CLOUDFLARE_API_KEY" \
             -H "Content-Type: application/json" | json-query "success"
    )
    [ "$SUCCESS" != "true" ] && error "DNS record delete failed."
    
    info "Fetching EC2 instance ID for $(instance-name)..."
    INSTANCE_ID=$(aws ec2 describe-instances --region $AWS_REGION --filters "Name=tag:Name,Values=$(instance-name)" --output text --query "Reservations[0].Instances[0].InstanceId" 2>/dev/null)
    
    [ -z "$INSTANCE_ID" ] && error "Failed to find EC2 instance named: $(instance-name)"
    
    info "Terminating EC2 $INSTANCE_ID..."
    SUCCESS=$( 
        aws ec2 terminate-instances --region $AWS_REGION --instance-ids $INSTANCE_ID | json-query "TerminatingInstances[0].CurrentState.Name" 
    )
    [ "$SUCCESS" != "shutting-down" ] && error "Termination failed. CurrentState=$SUCCESS"

    info "Feature server deleted successful."
}

function warm-up {
	INITIAL_DIR=$PWD
	cd $(product-dir)
	ant -f Build.xml warmup -Dou.home=$OU_HOME -Dwebdav.host=$(domain-name) -Dwebdav.port=443 -Dwebdav.protocol=https -Dwebdav.username=$(username) -Dwebdav.password=$(password) -Duse.os.get=yes
	cd $INITIAL_DIR
}

function instance-name {
    [ -z "$ARG_branch" ] && usage "-branch=<branch name> not specified"
    echo "INS $ARG_branch"
}

function branch-host-name {
    [ -z "$ARG_branch" ] && usage "-branch=<branch name> not specified"
    echo "$(echo ${ARG_branch/\//-} | tr '[:upper:]' '[:lower:]')"
} 

function domain-name {
    echo $(branch-host-name)$DOMAIN_NAME_SUFFIX
}

function product-dir {
	[ -z "$ARG_dir" ] && usage "-dir=<product directory> not specified"
	echo $ARG_dir
}

function username {
	[ -z "$ARG_username" ] && usage "-user=<ou username> not specified"
	echo $ARG_username
}

function password {
	[ -z "$ARG_password" ] && usage "-password=<ou password> not specified"
	echo $ARG_password
}

function service-name {
    [ -z "$ARG_service" ] && usage "-service=<service name> not specified"
    echo "$ARG_service"
} 

function load-feature-server-aws-env {
    [ -z "$FEATURE_SERVER_AWS_REGION" ] && usage "FEATURE_SERVER_AWS_REGION environment variable is not defined (or in ~/.ou)"
    [ -z "$FEATURE_SERVER_AWS_ACCESS_KEY_ID" ] && usage "FEATURE_SERVER_AWS_ACCESS_KEY_ID environment variable is not defined (or in ~/.ou)"
    [ -z "$FEATURE_SERVER_AWS_SECRET_ACCESS_KEY" ] && usage "FEATURE_SERVER_AWS_SECRET_ACCESS_KEY environment variable is not defined (or in ~/.ou)"
    
    export AWS_REGION=$FEATURE_SERVER_AWS_REGION
    export AWS_ACCESS_KEY_ID=$FEATURE_SERVER_AWS_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$FEATURE_SERVER_AWS_SECRET_ACCESS_KEY
}

