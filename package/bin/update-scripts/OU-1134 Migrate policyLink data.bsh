import java.net.*;
import java.util.*;
import java.lang.*;
import java.sql.*;
import java.io.*;
import java.math.*;
import java.nio.file.*;
import java.util.logging.*;
import java.util.regex.*;
import com.ail.core.persistence.hibernate.HibernateSessionBuilder;
import org.hibernate.*;
import com.ail.core.*;
import com.ail.pageflow.*;
import com.ail.insurance.*;
import com.ail.financial.*;
import com.ail.insurance.policy.*;

main() {
    if (migrate(bsh.args[0], bsh.args[1])) {
        System.exit(0);
    }
    else {
        System.exit(1);
    }
}

boolean migrate(String jboss, String bin) {
    Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

    try {
        url = System.getenv("dbUrl");
        userName = System.getenv("dbUsername");
        password = System.getenv("dbPassword");

        config = new HashMap();

        config.put("hibernate.listeners.envers.autoRegister", "false");

        config.put("hibernate.connection.url", url);
        config.put("hibernate.connection.username", userName);
        config.put("hibernate.connection.password", password);
        config.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
        config.put("hibernate.connection.current_session_context_class", "thread");
        config.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        config.put("hibernate.connection.pool_size", "1");
        config.put("hibernate.bytecode.use_reflection_optimizer", "true");
        config.put("hibernate.show_sql", "false");
        config.put("hibernate.format_sql", "true");

        migrateLiveData(config);        
        migrateAuditData(config);

        return true;
    } catch (Throwable e) {
        print("Exception: OU-1134 Migrate policyLink data " + e.getMessage());
        e.printStackTrace();
        return false;
    }
}

void migrateLiveData(Map config) throws Throwable {
    core = new CoreProxy();

    session = HibernateSessionBuilder.getSessionFactory(config).openSession();

    rawDataQuery = session.createSQLQuery("select polUID, polPolicyLink, polCreatedBy, polCreatedDate, polUpdatedBy, polUpdatedDate from polPolicy where polPolicyLink is not null;"); 

    results = rawDataQuery.list();
    rows = new ArrayList();
    for (result : results) {
        row = new HashMap();
        idx = 0;
        row.put("polUID", Long.valueOf(result[idx++].toString()));
        row.put("polPolicyLink", result[idx++]);
        rows.add(row);
    }
    session.close();

    session = HibernateSessionBuilder.getSessionFactory(config).openSession();
    tx = session.beginTransaction();

    for (row : rows) {
        polUID = row.get("polUID");
        policyLinksXML = row.get("polPolicyLink");
        
        policy = fetchPolicy(session, polUID);
        
        xmlString = new XMLString(policyLinksXML);
        policyLinks = core.fromXML(xmlString.getType(), xmlString);
        
        policy.setPolicyLink(policyLinks);

        session.flush();
        session.clear();
    }

    tx.commit();
    session.close();
}

void migrateAuditData(Map config) throws Throwable {
    pliUID = 1;
    core = new CoreProxy();

    session = HibernateSessionBuilder.getSessionFactory(config).openSession();

    rawDataQuery = session.createSQLQuery("select rev, revType, polUID, polPolicyLink from polPolicy_ where polPolicyLink is not null;"); 

    results = rawDataQuery.list();
    rows = new ArrayList();
    for (result : results) {
        row = new HashMap();
        idx = 0;
        row.put("rev", Long.valueOf(result[idx++].toString()));
        row.put("revType", Long.valueOf(result[idx++].toString()));
        row.put("polUID", Long.valueOf(result[idx++].toString()));
        row.put("polPolicyLink", result[idx++]);
        rows.add(row);
    }
    session.close();

    session = HibernateSessionBuilder.getSessionFactory(config).openSession();
    tx = session.beginTransaction();

    pliInsert = session.createSQLQuery(
        "insert into pliPolicyLink_ "+
        "(rev, revType, pliUID, pliAttribute, pliExternalSystemId, pliLinkType, pliPolicyLinkUIDpol, pliTargetPolicyUIDpol) "+
        "values "+
        "(:rev ,:revType, :pliUID, :pliAttribute, :pliExternalSystemId, :pliLinkType, :pliPolicyLinkUIDpol, :pliTargetPolicyUIDpol)"
    );
    
    jPliInsert = session.createSQLQuery(
        "insert into jPolPliPli_ "+
        "(rev, revType, pliPolicyLinkUIDpol, pliUID) "+
        "values "+
        "(:rev, :revType, :pliPolicyLinkUIDpol, :pliUID)"
    );
            
    for (row : rows) {
        rev = row.get("rev");
        revType = row.get("revType");
        polUID = row.get("polUID");
        policyLinksXML = row.get("polPolicyLink");
        
        xmlString = new XMLString(policyLinksXML);
        List policyLinks = core.fromXML(xmlString.getType(), xmlString);

        for(policyLink: policyLinks) {
            pliInsert.setParameter("rev", rev);
            pliInsert.setParameter("revType", revType);
            pliInsert.setParameter("pliUID", pliUID);
            pliInsert.setParameter("pliAttribute", core.toXML(policyLink.getAttribute()).toString());
            pliInsert.setParameter("pliExternalSystemId", UUID.randomUUID().toString());
            pliInsert.setParameter("pliLinkType", policyLink.getLinkType().toString());
            pliInsert.setParameter("pliTargetPolicyUIDpol", policyLink.getTargetPolicyId());
            pliInsert.setParameter("pliPolicyLinkUIDpol", polUID);
            pliInsert.executeUpdate(); 
            
            jPliInsert.setParameter("rev", rev);
            jPliInsert.setParameter("revType", revType);
            jPliInsert.setParameter("pliPolicyLinkUIDpol", polUID);
            jPliInsert.setParameter("pliUID", pliUID);
            jPliInsert.executeUpdate();
            
            pliUID = pliUID + 1;
        }

        session.flush();
        session.clear();
    }

    tx.commit();
    session.close();
}

fetchPolicy(session, polUID) {
    return session.getNamedQuery("get.policy.by.systemId").setParameter(0, polUID).list().get(0);
}

String readFile(String file) {
    line = null;
    stringBuilder = new StringBuilder();
    reader = new BufferedReader(new FileReader(file));
    while((line = reader.readLine()) != null) {
        stringBuilder.append(line);
    }
    return stringBuilder.toString();
}

main();