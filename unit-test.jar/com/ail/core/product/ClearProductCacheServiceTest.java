package com.ail.core.product;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.Core;
import com.ail.core.CoreProxy;
import com.ail.core.PostconditionException;
import com.ail.core.PreconditionException;
import com.ail.core.document.XDocCacheClearService.XDocCacheClearCommand;
import com.ail.core.product.ClearProductCacheService.ClearProductCacheArgument;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ClearProductCacheService.class})
public class ClearProductCacheServiceTest {
    private ClearProductCacheService sut = null;

    @Mock
    private Core core = null;
    @Mock
    private CoreProxy coreProxy = null;
    @Mock
    private ClearProductCacheArgument args = null;
    @Mock
    private XDocCacheClearCommand xdocCacheClearCommand;

    @Before
    public void setupSUT() throws Exception {
        MockitoAnnotations.initMocks(this);

        whenNew(CoreProxy.class).withNoArguments().thenReturn(coreProxy);

        doReturn(xdocCacheClearCommand).when(coreProxy).newCommand(eq(XDocCacheClearCommand.class));

        sut = new ClearProductCacheService();
        sut.setCore(core);
        sut.setArgs(args);

        when(args.getProductNameArg()).thenReturn("AIL.MADEUP.NAMESPACE");
    }

    @Test(expected=PreconditionException.class)
    public void testNullProductName() throws Exception {
        when(args.getProductNameArg()).thenReturn(null);
        sut.invoke();
    }

    @Test(expected=PreconditionException.class)
    public void testZeroLengthProductName() throws Exception {
        when(args.getProductNameArg()).thenReturn("");
        sut.invoke();
    }

    @Test(expected=PostconditionException.class)
    public void testNullResultFromCore() throws Exception {
        when(args.getProductNameArg()).thenReturn("rubbish");
        when(args.getNamespacesRet()).thenReturn(null);
        sut.invoke();
    }

    @Test
    public void testHappyPath() throws Exception {
        List<String> sampleResult=Arrays.asList("ONE", "TWO", "THREE");
        when(core.clearConfigurationCache(eq("AIL.MADEUP.NAMESPACE.Registry"))).thenReturn(sampleResult);
        sut.invoke();
        verify(args).setNamespacesRet(eq(sampleResult));
    }

    @Test
    public void confirmThatXDocCacheClearServiceIsInvoked() throws BaseException {
        sut.invoke();

        verify(xdocCacheClearCommand).invoke();
    }
}
