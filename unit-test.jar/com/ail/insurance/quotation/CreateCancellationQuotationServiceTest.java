package com.ail.insurance.quotation;

import static com.ail.insurance.policy.PolicyLinkType.CANCELLATION_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.CANCELLATION_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.RENEWAL_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.NOT_TAKEN_UP;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static com.ail.insurance.policy.PolicyStatus.QUOTATION;
import static com.ail.insurance.policy.PolicyStatus.SUBMITTED;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.quotation.CreateCancellationQuotationService.CreateCancellationQuotationArgument;
import com.ail.insurance.quotation.InitialiseCancellationService.InitialiseCancellationCommand;
import com.ail.insurance.quotation.PrepareRequoteService.PrepareRequoteCommand;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CoreContext.class, CreateCancellationQuotationService.class})
public class CreateCancellationQuotationServiceTest {
    private static final long CANCELLATION_SYSTEM_ID = 321L;
    private static final long POLICY_SYSTEM_ID = 1234L;

    private CreateCancellationQuotationService sut;

    @Mock
    private CreateCancellationQuotationArgument args;
    @Mock
    private Policy policy;
    @Mock
    private Policy cancellation;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private PrepareRequoteCommand prepareRequoteCommand;
    @Mock
    private InitialiseCancellationCommand initialiseCancellationCommand;

    private List<PolicyLink> policyPolicyLinks = new ArrayList<>();

    private List<PolicyLink>  cancellationPolicyLinks = new ArrayList<>();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        sut = new CreateCancellationQuotationService();
        sut.setArgs(args);

        mockStatic(CoreContext.class);
        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);

        doReturn(prepareRequoteCommand).when(coreProxy).newCommand(eq(PrepareRequoteCommand.class));
        doReturn(cancellation).when(prepareRequoteCommand).getRequoteRet();
        doReturn(initialiseCancellationCommand).when(coreProxy).newCommand(eq("InitialiseCancellation"), eq(InitialiseCancellationCommand.class));

        doReturn(policy).when(args).getPolicyArg();
        doReturn(ON_RISK).when(policy).getStatus();
        doReturn(policyPolicyLinks).when(policy).getPolicyLink();
        doReturn(POLICY_SYSTEM_ID).when(policy).getSystemId();

        doReturn(cancellation).when(args).getCancellationRet();
        doReturn(cancellation).when(coreProxy).create(eq(cancellation));
        doReturn(CANCELLATION_SYSTEM_ID).when(cancellation).getSystemId();
        doReturn(cancellationPolicyLinks).when(cancellation).getPolicyLink();

    }

    @Test(expected = PreconditionException.class)
    public void verifyPolicyArgCannotBeNull() throws BaseException {
        doReturn(null).when(args).getPolicyArg();
        sut.invoke();
    }

    @Test()
    public void verifyPolicyArgStatusMustBeOnRisk() throws BaseException {
        Stream.of(PolicyStatus.values()).
        filter(s -> s != ON_RISK).
        forEach(status -> {
            doReturn(status).when(policy).getStatus();
            try {
                sut.invoke();
                fail("PolicyStatus: '"+status+"' was accepted.");
             } catch (BaseException e) {
                 // ignore this, it is what we want.
             }
        });
    }

    @Test
    public void confirmOutstandingQuotationsAreMarkedAsNotTakenUp() throws BaseException {
        Long policySystemIdForMTALink1 = 123L;
        Long policySystemIdForMTALink2 = 124L;
        Long policySystemIdRenewalLink = 125L;
        PolicyLink policyLink;

        policyLink = mock(PolicyLink.class);
        doReturn(policySystemIdForMTALink1).when(policyLink).getTargetPolicyId();
        doReturn(MTA_QUOTATION_FOR).when(policyLink).getLinkType();
        policyPolicyLinks.add(policyLink);

        policyLink = mock(PolicyLink.class);
        doReturn(policySystemIdForMTALink2).when(policyLink).getTargetPolicyId();
        doReturn(MTA_QUOTATION_FOR).when(policyLink).getLinkType();
        policyPolicyLinks.add(policyLink);

        policyLink = mock(PolicyLink.class);
        doReturn(policySystemIdRenewalLink).when(policyLink).getTargetPolicyId();
        doReturn(RENEWAL_QUOTATION_FOR).when(policyLink).getLinkType();
        policyPolicyLinks.add(policyLink);

        Policy linkedPolicy1 = mock(Policy.class);
        doReturn(QUOTATION).when(linkedPolicy1).getStatus();
        doReturn(linkedPolicy1).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(policySystemIdForMTALink1));

        Policy linkedPolicy2 = mock(Policy.class);
        doReturn(QUOTATION).when(linkedPolicy2).getStatus();
        doReturn(linkedPolicy2).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(policySystemIdForMTALink2));

        Policy linkedPolicy3 = mock(Policy.class);
        doReturn(QUOTATION).when(linkedPolicy3).getStatus();
        doReturn(linkedPolicy3).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(policySystemIdRenewalLink));

        sut.invoke();

        verify(linkedPolicy1).setStatus(eq(NOT_TAKEN_UP));
        verify(linkedPolicy2).setStatus(eq(NOT_TAKEN_UP));
        verify(linkedPolicy3).setStatus(eq(NOT_TAKEN_UP));
    }

    @Test(expected = PreconditionException.class)
    public void verifyThatExtantSubmittedQuotationsBlockCancellation() throws BaseException {
        Long policySystemIdForMTALink1 = 123L;
        PolicyLink policyLink;

        policyLink = mock(PolicyLink.class);
        doReturn(policySystemIdForMTALink1).when(policyLink).getTargetPolicyId();
        doReturn(MTA_QUOTATION_FOR).when(policyLink).getLinkType();
        policyPolicyLinks.add(policyLink);

        Policy linkedPolicy1 = mock(Policy.class);
        doReturn(SUBMITTED).when(linkedPolicy1).getStatus();
        doReturn(linkedPolicy1).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(policySystemIdForMTALink1));

        sut.invoke();
    }

    @Test
    public void confirmPrepareRequoteServiceArgsAreCorrect() throws BaseException {
        sut.invoke();

        verify(prepareRequoteCommand).setPolicyArg(eq(policy));
        verify(prepareRequoteCommand).setSuppressDocumentCopyArg(eq(true));
        verify(prepareRequoteCommand).invoke();
    }

    @Test
    public void conformThatRequotePolicyIsReturned() throws BaseException {
        sut.invoke();
        verify(args).setCancellationRet(eq(cancellation));
    }

    @Test
    public void confirmPolicyLinksAreCorrectlySetup() throws BaseException {
        sut.invoke();

        assertThat(policyPolicyLinks, contains(new PolicyLink(CANCELLATION_QUOTATION_FOR, CANCELLATION_SYSTEM_ID)));
        assertThat(cancellationPolicyLinks , contains(new PolicyLink(CANCELLATION_QUOTATION_FROM, POLICY_SYSTEM_ID)));
    }

    @Test
    public void confirmInitialiseRenewalCommandIsInvoked() throws BaseException {
        sut.invoke();
        verify(initialiseCancellationCommand).invoke();
    }

    @Test
    public void confirmQuotationStatusIsSetToApplication() throws BaseException {
        sut.invoke();
        verify(cancellation).setStatus(eq(APPLICATION));
    }
}
