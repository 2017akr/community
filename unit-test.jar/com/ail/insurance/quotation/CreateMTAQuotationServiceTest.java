package com.ail.insurance.quotation;

import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FOR;
import static com.ail.insurance.policy.PolicyLinkType.MTA_QUOTATION_FROM;
import static com.ail.insurance.policy.PolicyStatus.APPLICATION;
import static com.ail.insurance.policy.PolicyStatus.NOT_TAKEN_UP;
import static com.ail.insurance.policy.PolicyStatus.ON_RISK;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.Core;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.policy.PolicyLink;
import com.ail.insurance.policy.PolicyStatus;
import com.ail.insurance.quotation.CreateMTAQuotationService.CreateMTAQuotationArgument;
import com.ail.insurance.quotation.InitialiseMTAService.InitialiseMTACommand;
import com.ail.insurance.quotation.PrepareRequoteService.PrepareRequoteCommand;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CoreContext.class, CreateMTAQuotationService.class})
public class CreateMTAQuotationServiceTest {

    private static final long MTA_SYSTEM_ID = 321L;

    private static final long POLICY_SYSTEM_ID = 1234L;

    private CreateMTAQuotationService sut;

    @Mock
    private CreateMTAQuotationArgument args;
    @Mock
    private Policy policy;
    @Mock
    private Policy mta;
    @Mock
    private PolicyLink mtaLink;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private Core core;
    @Mock
    private PrepareRequoteCommand prepareRequoteCommand;
    @Mock
    private InitialiseMTACommand initialiseMTACommand;

    private List<PolicyLink> policyPolicyLinks = new ArrayList<>();

    private List<PolicyLink> mtaPolicyLinks = new ArrayList<>();


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockStatic(CoreContext.class);

        sut = spy(new CreateMTAQuotationService());
        sut.setArgs(args);

        doReturn(core).when(sut).getCore();

        doReturn(policy).when(args).getPolicyArg();
        doReturn(ON_RISK).when(policy).getStatus();
        doReturn(POLICY_SYSTEM_ID).when(policy).getSystemId();
        doReturn(mta).when(args).getMTARet();
        doReturn(mta).when(coreProxy).create(eq(mta));

        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);

        doReturn(prepareRequoteCommand).when(coreProxy).newCommand(eq(PrepareRequoteCommand.class));
        doReturn(mta).when(prepareRequoteCommand).getRequoteRet();
        doReturn(MTA_SYSTEM_ID).when(mta).getSystemId();

        doReturn(initialiseMTACommand).when(coreProxy).newCommand(eq("InitialiseMTA"), eq(InitialiseMTACommand.class));

        doReturn(policyPolicyLinks).when(policy).getPolicyLink();
        doReturn(mtaPolicyLinks).when(mta).getPolicyLink();
    }

    @Test(expected = PreconditionException.class)
    public void confirmPolicyArgCannotBeNull() throws BaseException {
        doReturn(null).when(args).getPolicyArg();
        sut.invoke();
    }

    @Test
    public void confirmPolicyArgStatusCannotBeValuesOtherThanOnRisk() throws BaseException {
        Stream.of(PolicyStatus.values()).
               filter(s -> s != ON_RISK).
               forEach(status -> {
                   doReturn(status).when(policy).getStatus();
                   try {
                       sut.invoke();
                       fail("PolicyStatus: '"+status+"' was accepted.");
                    } catch (BaseException e) {
                        // ignore this, it is what we want.
                    }
               });
    }

    @Test
    public void confirmThatExistingMTAQuotationsAreMarkedAsNotTakenUp() throws BaseException {
        Long policySystemIdForMTALink = 123L;

        PolicyLink mtaLink = mock(PolicyLink.class);
        doReturn(policySystemIdForMTALink).when(mtaLink).getTargetPolicyId();
        doReturn(MTA_QUOTATION_FOR).when(mtaLink).getLinkType();
        policyPolicyLinks.add(mtaLink);

        Policy linkedPolicy = mock(Policy.class);
        doReturn(linkedPolicy).when(coreProxy).queryUnique(eq("get.policy.by.systemId"), eq(policySystemIdForMTALink));

        sut.invoke();

        verify(linkedPolicy).setStatus(eq(NOT_TAKEN_UP));
    }

    @Test
    public void confirmPrepareRequoteServiceArgsAreCorrect() throws BaseException {
        sut.invoke();

        verify(prepareRequoteCommand).setPolicyArg(eq(policy));
        verify(prepareRequoteCommand).setSuppressDocumentCopyArg(eq(true));
        verify(prepareRequoteCommand).invoke();
    }

    @Test
    public void conformThatRequotePolicyIsReturned() throws BaseException {
        sut.invoke();
        verify(args).setMTARet(eq(mta));
    }

    @Test
    public void confirmPolicyLinksAreCorrectlySetup() throws BaseException {
        sut.invoke();

        assertThat(policyPolicyLinks, contains(new PolicyLink(MTA_QUOTATION_FOR, MTA_SYSTEM_ID)));
        assertThat(mtaPolicyLinks, contains(new PolicyLink(MTA_QUOTATION_FROM, POLICY_SYSTEM_ID)));
    }

    @Test
    public void confirmInitialiseRenewalCommandIsInvoked() throws BaseException {
        sut.invoke();
        verify(initialiseMTACommand).invoke();
    }

    @Test
    public void confirmQuotationStatusIsSetToApplication() throws BaseException {
        sut.invoke();
        verify(mta).setStatus(eq(APPLICATION));
    }

}
