package com.ail.pageflow.service;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ail.core.BaseException;
import com.ail.core.CoreContext;
import com.ail.core.CoreProxy;
import com.ail.core.PreconditionException;
import com.ail.core.context.PreferencesWrapper;
import com.ail.insurance.policy.Policy;
import com.ail.insurance.quotation.CreateMTAQuotationService.CreateMTAQuotationCommand;
import com.ail.pageflow.ExecutePageActionService.ExecutePageActionArgument;
import com.ail.pageflow.PageFlowContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({PageFlowContext.class, CoreContext.class, CreateMTAQuotationService.class})
public class CreateMTAQuotationServiceTest {

    private CreateMTAQuotationService sut;

    @Mock
    private ExecutePageActionArgument args;
    @Mock
    private Policy policy;
    @Mock
    private Policy mta;
    @Mock
    private CoreProxy coreProxy;
    @Mock
    private CreateMTAQuotationCommand createMTAQuotationCommand;
    @Mock
    private PreferencesWrapper preferencesWrapper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mockStatic(PageFlowContext.class);
        mockStatic(CoreContext.class);

        when(CoreContext.getCoreProxy()).thenReturn(coreProxy);
        when(CoreContext.getPreferencesWrapper()).thenReturn(preferencesWrapper);

        doReturn(policy).when(args).getModelArgRet();
        doReturn(createMTAQuotationCommand).when(coreProxy).newCommand(eq(CreateMTAQuotationCommand.class));
        doReturn(mta).when(createMTAQuotationCommand).getMTARet();

        sut = new CreateMTAQuotationService();
        sut.setArgs(args);
    }

    @Test(expected = PreconditionException.class)
    public void confirmNullPolicyCausesException() throws BaseException {
        when(args.getModelArgRet()).thenReturn(null);
        sut.invoke();
    }

    @Test
    public void confirmCreateMTAQuotationCommandIsInvoked() throws BaseException {
        sut.invoke();

        verify(createMTAQuotationCommand).setPolicyArg(eq(policy));
        verify(createMTAQuotationCommand).invoke();
    }
}
